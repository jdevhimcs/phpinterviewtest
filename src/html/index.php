<?php
require_once "../bootstrap.php";
$oMaster = init();

if($oMaster->oSessionMan->checkSession()){
    header('Location: /html/todos/');
    die();
}
header('Location: /html/login/');
