//This is called when the checkbox is ticket in the todos page
function markDone(id){
    console.log("CLICKED ON "+id)
    var r = confirm("Are you sure, you want to change status!");
    if (r == true) {
        //TODO complete this section to call backend and refresh page.
        $.post('?action=done', {
            id: id
        }, function(data){
            alert(data);
            location.reload();
            
        }).fail(function() {
            alert('Unable to update.');
        });
    }else{
        $('input[type=checkbox]').removeAttr('checked',false);
    } 
}