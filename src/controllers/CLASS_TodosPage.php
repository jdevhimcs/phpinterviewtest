<?php
class CTodosPage extends CController {
    /**
     * @var CTodosView
     */
    private $oView;
    /**
     * @var CTodos
     */
    private $oModel;

    public function __construct(CMaster $oMaster)
    {
        parent::__construct($oMaster, ["index", "create", "done"], "index");

        // check session is exist or not
        if($_SESSION['loggedIn'] != true){
            header('Location: /html/login/');
        }

        include_once  ROOT_DIR."/models/CLASS_Todos.php";
        $this->oModel = new CTodos($oMaster);

        include_once ROOT_DIR . "/views/CLASS_TodosView.php";
        $this->oView = new CTodosView();

    }

    public function index($arrGets, $arrPosts){
        $arrTodos = $this->oModel->listTodos();

        echo $this->oMaster->oView->headers();
        echo $this->oView->listTodos($arrTodos);
        echo $this->oMaster->oView->footer();
    }

    public function create($arrGets, $arrPosts){    	
        //TODO complete this section to add a todo item
        $oTodo = $this->oModel->createTodo($arrPosts['title']);
        if(!$oTodo){
            $strErrors = "Oops! Something went wrong, please try again";
        }else{
            $strErrors = "Successfully inserted";
        }

        header('Location: /html/todos/');
    }

    public function done($arrGets, $arrPosts){
        //TODO complete this section to mark a todo item as done
        $this->oModel->markDone($arrPosts['id']);             
        echo 'Updated Successfully!';   
    }
}
