<?php

class CTodosView extends CView {

    public function listTodos(array $arrTodos): string {
       $strHTML = "<h1>Todos</h1>";

       $strHTML .= "<div>";
       $strHTML .= "<h2>Remaining:</h2>";

        /** @var CTodo $oTodo */
       foreach ($arrTodos as  $oTodo){
           if($oTodo->bCompleted) continue;

           $strHTML .= "<div><span>". $oTodo->strTitle ."</span> <input type='checkbox' onclick='markDone(\"{$oTodo->intId}\")'></div>";
       }
       $strHTML .= "</div>";

        $strHTML .= "<div>";
        $strHTML .= "<h2>Done:</h2>";
        /** @var CTodo $oTodo */
        foreach ($arrTodos as  $oTodo){
            if(!$oTodo->bCompleted) continue;

            $strHTML .= "<div><span>". $oTodo->strTitle ."</span></div>";
        }
        $strHTML .= "</div>";

        //TODO add form for submitting new todo items here
        $strHTML .= '<div class="create-page">
        <div class="form">
        <div><span style="background: lightcoral">'.$strErrors.'</span>   </div>
        <div>Create todo from here</div><br><br>
            <form class="todo-form" method="post" action="?action=create" >
                <input name="title" type="text" placeholder="New Todo Title" />
                <button type="submit">create</button>
            </form>
        </div>
    </div>';

        $strHTML .= '<script language="javascript" type="text/javascript" src="/html/js/todos/todo.js"></script>';
       return $strHTML;
    }
}
